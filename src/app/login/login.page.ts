import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private router: Router, public toastController: ToastController) { }

  public form={
    reference:"",
    password:"",
  }

  ngOnInit() {
  }

  async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,      
      duration: 2000
    });
    toast.present();
  }
  post(){
      // console.log(this.form)
      // this.router.navigate(['/usr/home'])

      if(localStorage.getItem('Reference') == this.form.reference && localStorage.getItem('Password') == this.form.password){
        this.router.navigate(['/tabs/home'])
        localStorage.setItem("isLogin","true")
      }
      else{
        this.presentToast("Invalid Reference or Password");
      }
      if(this.form.reference === "" || this.form.password === ""){
         this.presentToast("Cannot accept empty field");
      }
    }
}
