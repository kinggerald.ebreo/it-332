import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  public fname:string =""
  public ref: string =""

  public contact: string =""
  public email: string =""
  public address: string =""
  public counselor: string =""
  public date: string =""
  public description: string =""

  public user_info={
    contact: "",
    email:"",
    address:"",
    counselor:"",
    date:"",
    description:""

  }
  public slideOpts = {
    slidesPerView: 1,
    spaceBetween: 30,
    // centeredSlides: true,
    initialSlide: 1,
    coverflowEffect: {
      rotate: 0,
      stretch: 0,
      depth: 200,
      modifier: 1,
      slideShadows: true,
    },
    on: {
      beforeInit() {
        const swiper = this;
  
        swiper.classNames.push(`${swiper.params.containerModifierClass}coverflow`);
        swiper.classNames.push(`${swiper.params.containerModifierClass}3d`);
  
        swiper.params.watchSlidesProgress = true;
        swiper.originalParams.watchSlidesProgress = true;
      },
      setTranslate() {
        const swiper = this;
        const {
          width: swiperWidth, height: swiperHeight, slides, $wrapperEl, slidesSizesGrid, $
        } = swiper;
        const params = swiper.params.coverflowEffect;
        const isHorizontal = swiper.isHorizontal();
        const transform$$1 = swiper.translate;
        const center = isHorizontal ? -transform$$1 + (swiperWidth / 2) : -transform$$1 + (swiperHeight / 2);
        const rotate = isHorizontal ? params.rotate : -params.rotate;
        const translate = params.depth;
        // Each slide offset from center
        for (let i = 0, length = slides.length; i < length; i += 1) {
          const $slideEl = slides.eq(i);
          const slideSize = slidesSizesGrid[i];
          const slideOffset = $slideEl[0].swiperSlideOffset;
          const offsetMultiplier = ((center - slideOffset - (slideSize / 2)) / slideSize) * params.modifier;
  
           let rotateY = isHorizontal ? rotate * offsetMultiplier : 0;
          let rotateX = isHorizontal ? 0 : rotate * offsetMultiplier;
          // var rotateZ = 0
          let translateZ = -translate * Math.abs(offsetMultiplier);
  
           let translateY = isHorizontal ? 0 : params.stretch * (offsetMultiplier);
          let translateX = isHorizontal ? params.stretch * (offsetMultiplier) : 0;
  
           // Fix for ultra small values
          if (Math.abs(translateX) < 0.001) translateX = 0;
          if (Math.abs(translateY) < 0.001) translateY = 0;
          if (Math.abs(translateZ) < 0.001) translateZ = 0;
          if (Math.abs(rotateY) < 0.001) rotateY = 0;
          if (Math.abs(rotateX) < 0.001) rotateX = 0;
  
           const slideTransform = `translate3d(${translateX}px,${translateY}px,${translateZ}px)  rotateX(${rotateX}deg) rotateY(${rotateY}deg)`;
  
           $slideEl.transform(slideTransform);
          $slideEl[0].style.zIndex = -Math.abs(Math.round(offsetMultiplier)) + 1;
          if (params.slideShadows) {
            // Set shadows
            let $shadowBeforeEl = isHorizontal ? $slideEl.find('.swiper-slide-shadow-left') : $slideEl.find('.swiper-slide-shadow-top');
            let $shadowAfterEl = isHorizontal ? $slideEl.find('.swiper-slide-shadow-right') : $slideEl.find('.swiper-slide-shadow-bottom');
            if ($shadowBeforeEl.length === 0) {
              $shadowBeforeEl = swiper.$(`<div class="swiper-slide-shadow-${isHorizontal ? 'left' : 'top'}"></div>`);
              $slideEl.append($shadowBeforeEl);
            }
            if ($shadowAfterEl.length === 0) {
              $shadowAfterEl = swiper.$(`<div class="swiper-slide-shadow-${isHorizontal ? 'right' : 'bottom'}"></div>`);
              $slideEl.append($shadowAfterEl);
            }
            if ($shadowBeforeEl.length) $shadowBeforeEl[0].style.opacity = offsetMultiplier > 0 ? offsetMultiplier : 0;
            if ($shadowAfterEl.length) $shadowAfterEl[0].style.opacity = (-offsetMultiplier) > 0 ? -offsetMultiplier : 0;
          }
        }
  
         // Set correct perspective for IE10
        if (swiper.support.pointerEvents || swiper.support.prefixedPointerEvents) {
          const ws = $wrapperEl[0].style;
          ws.perspectiveOrigin = `${center}px 50%`;
        }
      },
      setTransition(duration) {
        const swiper = this;
        swiper.slides
          .transition(duration)
          .find('.swiper-slide-shadow-top, .swiper-slide-shadow-right, .swiper-slide-shadow-bottom, .swiper-slide-shadow-left')
          .transition(duration);
      }
    }
  }
  
  
  // public contact = '';
  // public c1 = '';
  // public email = '';
  // public c2 = '';
  // public address = '';
  // public c3 = '';
  // public counselor = '';
  // public c4 = '';
  // public date = '';
  // public c5 = '';
  // public description = '';
  // public c6 = '';
  // public checkbox = false; 


  constructor(private router:Router, public loadingController: LoadingController, public toastController: ToastController) { 
    this.fname = localStorage.getItem('Full Name')
    this.ref = localStorage.getItem('Reference')


    this.contact = localStorage.getItem('Contact No.')
    this.email = localStorage.getItem('Email Address')
    this.address = localStorage.getItem('Address')
    this.counselor = localStorage.getItem('Financial Counselor')
    this.date = localStorage.getItem('Date of Registration')
    this.description = localStorage.getItem('Description')

  }
  

  // clickCheckBox(){
  //   this.checkbox = !this.checkbox;

  //   if(this.checkbox == true){
  //     this.contact = this.c1;
  //     this.email = this.c2;
  //     this.address = this.c3;
  //     this.counselor = this.c4;
  //     this.date = this.c5;
  //     this.description = this.c6;
  //   }

  //   if(this.checkbox == false){
  //     this.contact = '';
  //     this.email = '';
  //     this.address = '';
  //     this.counselor = '';
  //     this.date = '';
  //     this.description = '';
  //   }
  // }


  ngOnInit() {
    
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Added Successfuly.',      
      duration: 2000
    });
    toast.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2800
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
    this.router.navigate(['/tabs/home'])
    this.presentToast()
  }

  url="";

  onselectFile(e){
    if(e.target.files)
    var reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);
    reader.onload=(event:any)=>{
      this.url=event.target.result;
    }
  }

  user_infos(){
    localStorage.setItem('Contact No.', this.user_info.contact)
    localStorage.setItem('Email Address', this.user_info.email)
    localStorage.setItem('Address', this.user_info.address)
    localStorage.setItem('Financial Counselor', this.user_info.counselor)
    localStorage.setItem('Date of Registration', this.user_info.date)
    localStorage.setItem('Description', this.user_info.description)
    
    this.presentLoading()
        
  }
}
