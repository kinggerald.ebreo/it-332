import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  public signIn_user={
    full: "",
    address:"",
    province:"",
    municipality:"",
    reference:"",
    password:"",
    cpassword:""
  }

  constructor(private router:Router, public loadingController: LoadingController, public toastController: ToastController) { }

  ngOnInit() {
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Sign-Up Successfuly.',      
      duration: 2000
    });
    toast.present();
  }

  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2800
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
    this.router.navigate(['/login'])
    this.presentToast()
  }

  signUp(){
    localStorage.setItem('Full Name', this.signIn_user.full)
    localStorage.setItem('Address', this.signIn_user.address)
    localStorage.setItem('Province', this.signIn_user.province)
    localStorage.setItem('Municipality', this.signIn_user.municipality)
    localStorage.setItem('Reference', this.signIn_user.reference)
    localStorage.setItem('Password', this.signIn_user.password)
    localStorage.setItem('Confirm Password', this.signIn_user.cpassword)
    this.presentLoading()
        
  }
}
