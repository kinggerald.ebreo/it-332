import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage, 
    children:[
  {
    path: 'home',
    loadChildren: () => import('../home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'announcement',
    loadChildren: () => import('../announcement/announcement.module').then( m => m.AnnouncementPageModule)
  },
  {
    path: 'records',
    loadChildren: () => import('../records/records.module').then( m => m.RecordsPageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('../payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
